module Types exposing (Display(..))

type Display
    = Active | Hidden
